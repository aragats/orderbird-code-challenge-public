package com.orderbird.util;

import com.orderbird.entity.ReservationDO;
import com.orderbird.entity.TableDO;
import com.orderbird.enttityvalue.TimeSlot;
import org.junit.Assert;

import java.util.List;

public class MyOrderBirdAssert {

    // Theoretically, we can override equals() and hashCode() methods in TableDO but I do not want to rely on that.

    public static void assertEqualsTable(TableDO expected, TableDO actual, boolean deep) {
        Assert.assertNotNull(expected);
        Assert.assertNotNull(actual);
        Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getName(), actual.getName());
        if (deep) {
            if (expected.getReservations() != null || actual.getReservations() != null) {
                assertEqualsReservationList(expected.getReservations(), actual.getReservations());
            }
        }
    }

    public static void assertEqualsReservationList(List<ReservationDO> expected, List<ReservationDO> actual) {
        Assert.assertNotNull(expected);
        Assert.assertNotNull(actual);
        Assert.assertTrue(expected.size() == actual.size());
        for (int i = 0; i < expected.size(); i++) {
            assertEqualsReservation(expected.get(i), actual.get(i));
        }
    }

    public static void assertEqualsReservation(ReservationDO expected, ReservationDO actual) {
        Assert.assertNotNull(expected);
        Assert.assertNotNull(actual);
        Assert.assertEquals(expected.getId(), actual.getId());
        Assert.assertEquals(expected.getCustomerName(), actual.getCustomerName());
        if (expected.getTimeslot() != null || actual.getTimeslot() != null) {
            assertEqualsTimeslot(expected.getTimeslot(), actual.getTimeslot());
        }
    }

    public static void assertEqualsTimeslot(TimeSlot expected, TimeSlot actual) {
        Assert.assertNotNull(expected);
        Assert.assertNotNull(actual);
        Assert.assertEquals(expected.getFromTime(), actual.getFromTime());
        Assert.assertEquals(expected.getToTime(), actual.getToTime());

    }

}
