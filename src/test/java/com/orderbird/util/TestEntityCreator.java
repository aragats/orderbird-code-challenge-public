package com.orderbird.util;

import com.orderbird.entity.ReservationDO;
import com.orderbird.entity.TableDO;
import com.orderbird.enttityvalue.TimeSlot;

import java.time.ZonedDateTime;

public class TestEntityCreator {

    public static TableDO createTable() {
        TableDO expected = new TableDO("My test table");
        expected.setId(1L);
        return expected;
    }


    public static ReservationDO createReservation() {
        ReservationDO expected = new ReservationDO("My custom name", new TimeSlot(ZonedDateTime.now().plusHours(1), ZonedDateTime.now().plusHours(2)));
        expected.setId(1L);
        return expected;
    }


}
