package com.orderbird.contoller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orderbird.contoller.mapper.ReservationMapper;
import com.orderbird.contoller.mapper.TableMapper;
import com.orderbird.dto.ReservationDTO;
import com.orderbird.dto.TableDTO;
import com.orderbird.entity.ReservationDO;
import com.orderbird.entity.TableDO;
import com.orderbird.service.TableService;
import com.orderbird.util.TestEntityCreator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TableControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private TableService service;

    @Test
    public void testCreateTable() throws Exception {
        TableDO tableDO = TestEntityCreator.createTable();
        TableDTO tableDTO = TableMapper.makeTableDTO(tableDO);

        Mockito.when(service.create(Mockito.any(TableDO.class))).thenReturn(tableDO);

        ObjectMapper mapper = new ObjectMapper();

        MvcResult result = mvc.perform(post("/api/v1/table/")
                .contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(tableDTO)))
                .andExpect(status().isCreated()).andReturn();

        assertEquals(
                mapper.writeValueAsString(tableDTO),
                result.getResponse().getContentAsString());
    }

    @Test
    public void testCreateReservation() throws Exception {
        ReservationDO reservationDO = TestEntityCreator.createReservation();
        ReservationDTO reservationDTO = ReservationMapper.makeReservationDTO(reservationDO);

        doNothing().when(service);

        ObjectMapper mapper = new ObjectMapper();

        MvcResult result = mvc.perform(post("/api/v1/table/" + 1L + "/reservation")
                .contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(reservationDTO)))
                .andExpect(status().isCreated()).andReturn();

        assertEquals("", result.getResponse().getContentAsString());

    }

    @Test
    public void testFindTable() throws Exception {
        TableDO tableDO = TestEntityCreator.createTable();
        TableDTO tableDTO = TableMapper.makeTableDTO(tableDO);

        Mockito.when(service.findTableWithReservations(tableDTO.getId())).thenReturn(tableDO);

        ObjectMapper mapper = new ObjectMapper();

        MvcResult result = mvc.perform(get("/api/v1/table/" + tableDO.getId()))
                .andExpect(status().isOk()).andReturn();

        assertEquals(
                mapper.writeValueAsString(tableDTO),
                result.getResponse().getContentAsString());
    }
}
