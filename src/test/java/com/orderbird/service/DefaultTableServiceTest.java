package com.orderbird.service;

import com.orderbird.OrderBirdApplication;
import com.orderbird.dao.TableRepository;
import com.orderbird.entity.ReservationDO;
import com.orderbird.entity.TableDO;
import com.orderbird.exception.ConstraintsViolationException;
import com.orderbird.exception.EntityNotFoundException;
import com.orderbird.exception.NotValidReservationTime;
import com.orderbird.exception.TableAlreadyInUseException;
import com.orderbird.util.MyOrderBirdAssert;
import com.orderbird.util.TestEntityCreator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrderBirdApplication.class)
public class DefaultTableServiceTest {


    @MockBean
    private TableRepository repository;

    @Autowired
    private TableService service;


    @Test
    public void testFind() throws EntityNotFoundException {
        TableDO expected = TestEntityCreator.createTable();
        Mockito.when(repository.findOne(expected.getId())).thenReturn(expected);

        TableDO actual = service.find(expected.getId());
        MyOrderBirdAssert.assertEqualsTable(expected, actual, true);
    }

    @Test
    public void testCreate() throws ConstraintsViolationException {
        TableDO expected = TestEntityCreator.createTable();
        Mockito.when(repository.save(expected)).thenReturn(expected);

        TableDO actual = service.create(expected);
        MyOrderBirdAssert.assertEqualsTable(expected, actual, true);
    }


    @Test
    public void testMakeReservation() throws TableAlreadyInUseException, EntityNotFoundException, NotValidReservationTime {
        TableDO expectedTable = TestEntityCreator.createTable();
        ReservationDO expectedReservation = TestEntityCreator.createReservation();
        Mockito.when(repository.findOne(expectedTable.getId())).thenReturn(expectedTable);

        service.makeReservation(expectedTable.getId(), expectedReservation);
        Assert.assertNotNull(expectedTable.getReservations());
        Assert.assertTrue(expectedTable.getReservations().size() == 1);
        MyOrderBirdAssert.assertEqualsReservation(expectedReservation, expectedTable.getReservations().get(0));

    }

    @Test
    public void testFindTableWithReservations() throws EntityNotFoundException {

        TableDO expected = TestEntityCreator.createTable();
        expected.setReservations(Collections.singletonList(TestEntityCreator.createReservation()));
        Mockito.when(repository.findOne(expected.getId())).thenReturn(expected);

        TableDO actual = service.find(expected.getId());
        MyOrderBirdAssert.assertEqualsTable(expected, actual, true);
    }
}
