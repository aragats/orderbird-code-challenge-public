package com.orderbird.dao;

import com.orderbird.OrderBirdApplication;
import com.orderbird.entity.TableDO;
import com.orderbird.util.MyOrderBirdAssert;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrderBirdApplication.class)
public class TableRepositoryTest {


    @Autowired
    private TableRepository repository;


    @Test
    public void testSave() throws Exception {
        TableDO expected = new TableDO();
        expected.setName("My table A");

        TableDO actual = repository.save(expected);
        MyOrderBirdAssert.assertEqualsTable(expected, actual, false);
    }


    @Test
    public void testFindOne() throws Exception {
        TableDO expected = new TableDO();
        expected.setName("My table B");

        repository.save(expected);
        Assert.assertNotNull(expected.getId());
        TableDO actual = repository.findOne(expected.getId());
        MyOrderBirdAssert.assertEqualsTable(expected, actual, false);
    }


    @Test
    public void testDelete() throws Exception {
        TableDO expected = new TableDO();
        expected.setName("My table C");

        Assert.assertNotNull(repository.save(expected));
        Assert.assertNotNull(expected.getId());
        repository.delete(expected.getId());
        Assert.assertNull(repository.findOne(expected.getId()));
    }

}
