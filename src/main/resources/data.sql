/**
 * CREATE Script for init of DB
 */


-- Create 4 empty tables
INSERT INTO table (id, date_created, name)
VALUES (1, now(), 'Table 1');

INSERT INTO table (id, date_created, name)
VALUES (2, now(), 'Table 2');

INSERT INTO table (id, date_created, name)
VALUES (3, now(), 'Table 3');

INSERT INTO table (id, date_created, name)
VALUES (4, now(), 'Table 4');

-- Create 2 tables with reservations

INSERT INTO table (id, date_created, name)
VALUES (5, now(), 'Table 5');

INSERT INTO table (id, date_created, name)
VALUES (6, now(), 'Table 6');

-- Create 3 Reservations for tables 5 and 6

INSERT INTO reservation (id, date_created, customer_name, from_time, to_time, table_id)
VALUES (1, now(), 'Customer 1', now(), now() + 10, 5);

