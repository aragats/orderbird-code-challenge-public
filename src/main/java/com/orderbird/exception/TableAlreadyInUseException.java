package com.orderbird.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Table is already in use by another customer for that time ...")
public class TableAlreadyInUseException extends Exception {

    private static final long serialVersionUID = 1300835871726788977L;

    public TableAlreadyInUseException(String message) {
        super(message);
    }

}
