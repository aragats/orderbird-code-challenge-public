package com.orderbird.contoller.mapper;

import com.orderbird.dto.ReservationDTO;
import com.orderbird.entity.ReservationDO;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ReservationMapper {
    public static ReservationDO makeReservationDO(ReservationDTO reservationDTO) {
        return new ReservationDO(reservationDTO.getCustomerName().trim(), reservationDTO.getTimeslot());
    }


    public static ReservationDTO makeReservationDTO(ReservationDO reservationDO) {
        ReservationDTO.ReservationDTOBuilder reservationDTOBuilder = ReservationDTO.newBuilder()
                .setId(reservationDO.getId())
                .setCustomerName(reservationDO.getCustomerName())
                .setTimeslot(reservationDO.getTimeslot());
        return reservationDTOBuilder.createReservationDTO();
    }


    public static List<ReservationDTO> makeReservationDTOList(Collection<ReservationDO> reservations) {
        return reservations.stream()
                .map(ReservationMapper::makeReservationDTO)
                .collect(Collectors.toList());
    }
}
