package com.orderbird.contoller.mapper;

import com.orderbird.dto.TableDTO;
import com.orderbird.entity.TableDO;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class TableMapper {
    public static TableDO makeTableDO(TableDTO tableDTO) {
        return new TableDO(tableDTO.getName().trim());
    }


    public static TableDTO makeTableDTO(TableDO tableDO) {
        TableDTO.TableDTOBuilder tableDTOBuilder = TableDTO.newBuilder()
                .setId(tableDO.getId())
                .setName(tableDO.getName())
                .setReservations(ReservationMapper.makeReservationDTOList(tableDO.getReservations()));
        return tableDTOBuilder.createTableDTO();
    }


    public static List<TableDTO> makeTableDTOList(Collection<TableDO> tables) {
        return tables.stream()
                .map(TableMapper::makeTableDTO)
                .collect(Collectors.toList());
    }
}
