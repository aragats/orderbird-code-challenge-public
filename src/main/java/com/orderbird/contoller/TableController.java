package com.orderbird.contoller;

import com.orderbird.contoller.mapper.ReservationMapper;
import com.orderbird.contoller.mapper.TableMapper;
import com.orderbird.dto.ReservationDTO;
import com.orderbird.dto.TableDTO;
import com.orderbird.entity.ReservationDO;
import com.orderbird.entity.TableDO;
import com.orderbird.exception.ConstraintsViolationException;
import com.orderbird.exception.EntityNotFoundException;
import com.orderbird.exception.NotValidReservationTime;
import com.orderbird.exception.TableAlreadyInUseException;
import com.orderbird.service.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/table")
public class TableController {

    private final TableService tableService;


    @Autowired
    public TableController(final TableService tableService) {
        this.tableService = tableService;
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TableDTO createTable(@Valid @RequestBody TableDTO tableDTO) throws ConstraintsViolationException {
        TableDO tableDO = TableMapper.makeTableDO(tableDTO);
        return TableMapper.makeTableDTO(tableService.create(tableDO));
    }

    @PostMapping("/{tableId}/reservation")
    @ResponseStatus(HttpStatus.CREATED)
    public void createReservation(@Valid @PathVariable long tableId, @Valid @RequestBody ReservationDTO reservationDTO) throws EntityNotFoundException, TableAlreadyInUseException, NotValidReservationTime {
        ReservationDO reservationDO = ReservationMapper.makeReservationDO(reservationDTO);
        tableService.makeReservation(tableId, reservationDO);
    }


    @GetMapping("/{tableId}")
    public TableDTO findTable(@Valid @PathVariable long tableId) throws EntityNotFoundException {
        return TableMapper.makeTableDTO(tableService.findTableWithReservations(tableId));
    }


}
