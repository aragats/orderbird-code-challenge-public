package com.orderbird.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.util.LinkedList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TableDTO {

    @JsonIgnore
    private Long id;

    @NotNull(message = "Name can not be null!")
    private String name;

    @JsonIgnore
    private List<ReservationDTO> reservations = new LinkedList<>();

    private TableDTO() {
    }

    private TableDTO(Long id, String name, List<ReservationDTO> reservations) {
        this.id = id;
        this.name = name;
        this.reservations = reservations;
    }

    public static TableDTOBuilder newBuilder() {
        return new TableDTOBuilder();
    }

    @JsonProperty
    public Long getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    @JsonProperty
    public List<ReservationDTO> getReservations() {
        return reservations;
    }

    public static class TableDTOBuilder {
        private Long id;
        private String name;
        private List<ReservationDTO> reservations = new LinkedList<>();


        public TableDTOBuilder setId(Long id) {
            this.id = id;
            return this;
        }


        public TableDTOBuilder setName(String name) {
            this.name = name;
            return this;
        }


        public TableDTOBuilder setReservations(List<ReservationDTO> reservations) {
            this.reservations = reservations;
            return this;
        }


        public TableDTO createTableDTO() {
            return new TableDTO(id, name, reservations);
        }

    }

}
