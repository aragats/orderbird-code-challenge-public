package com.orderbird.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.orderbird.enttityvalue.TimeSlot;

import javax.validation.constraints.NotNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReservationDTO {

    @JsonIgnore
    private Long id;


    @NotNull(message = "CustomerName can not be null!")
    private String customerName;

    @NotNull(message = "Timeslot can not be null!")
    private TimeSlot timeslot;

    private ReservationDTO() {
    }

    private ReservationDTO(Long id, String customerName, TimeSlot timeslot) {
        this.id = id;
        this.customerName = customerName;
        this.timeslot = timeslot;
    }

    public static ReservationDTO.ReservationDTOBuilder newBuilder() {
        return new ReservationDTOBuilder();
    }

    @JsonProperty
    public Long getId() {
        return id;
    }


    public String getCustomerName() {
        return customerName;
    }


    public TimeSlot getTimeslot() {
        return timeslot;
    }


    public static class ReservationDTOBuilder {
        private Long id;
        private String customerName;
        private TimeSlot timeslot;


        public ReservationDTOBuilder setId(Long id) {
            this.id = id;
            return this;
        }


        public ReservationDTOBuilder setCustomerName(String customerName) {
            this.customerName = customerName;
            return this;
        }


        public ReservationDTOBuilder setTimeslot(TimeSlot timeslot) {
            this.timeslot = timeslot;
            return this;
        }

        public ReservationDTO createReservationDTO() {
            return new ReservationDTO(id, customerName, timeslot);
        }

    }
}
