package com.orderbird.entity;

import com.orderbird.enttityvalue.TimeSlot;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Entity
@Table(name = "reservation")
public class ReservationDO {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime dateCreated = ZonedDateTime.now();

    @Column(nullable = false, name = "customer_name")
    @NotNull(message = "Description can not be null!")
    private String customerName;

    @Embedded
    private TimeSlot timeslot;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "table_id", nullable = false)
    private TableDO table;

    public ReservationDO() {
    }

    public ReservationDO(String customerName, TimeSlot timeslot) {
        this.customerName = customerName;
        this.timeslot = timeslot;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public TimeSlot getTimeslot() {
        return timeslot;
    }

    public void setTimeslot(TimeSlot timeslot) {
        this.timeslot = timeslot;
    }

    public TableDO getTable() {
        return table;
    }

    public void setTable(TableDO table) {
        this.table = table;
    }
}
