package com.orderbird.service;

import com.orderbird.dao.TableRepository;
import com.orderbird.entity.ReservationDO;
import com.orderbird.entity.TableDO;
import com.orderbird.exception.ConstraintsViolationException;
import com.orderbird.exception.EntityNotFoundException;
import com.orderbird.exception.NotValidReservationTime;
import com.orderbird.exception.TableAlreadyInUseException;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;

@Service
public class DefaultTableService implements TableService {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(DefaultTableService.class);

    private final TableRepository tableRepository;


    public DefaultTableService(final TableRepository tableRepository) {
        this.tableRepository = tableRepository;
    }


    @Override
    public TableDO find(Long tableId) throws EntityNotFoundException {
        return findTableChecked(tableId);
    }

    @Override
    public TableDO create(TableDO tableDO) throws ConstraintsViolationException {
        TableDO table;
        try {
            table = tableRepository.save(tableDO);
        } catch (DataIntegrityViolationException e) {
            LOG.warn("Some constraints are thrown due to table creation", e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return table;
    }

    @Override
    @Transactional
    public void makeReservation(Long tableId, ReservationDO reservationDO) throws EntityNotFoundException, TableAlreadyInUseException, NotValidReservationTime {
        TableDO tableDO = findTableChecked(tableId);
        if (!isValidReservation(reservationDO)) {
            throw new NotValidReservationTime("The reservation time is not valid, from = " + reservationDO.getTimeslot().getFrom()
                    + ", to = " + reservationDO.getTimeslot().getTo());
        }
        if (!isValidReservationForTable(tableDO.getReservations(), reservationDO)) {
            throw new TableAlreadyInUseException("Table is already reserved for this time, tableId = " + tableId
                    + ", from = " + reservationDO.getTimeslot().getFrom() + ", to = " + reservationDO.getTimeslot().getTo());
        }
        reservationDO.setTable(tableDO);
        tableDO.getReservations().add(reservationDO);
    }

    @Override
    public TableDO findTableWithReservations(Long tableId) throws EntityNotFoundException {
        TableDO tableDO = findTableChecked(tableId);
        tableDO.getReservations().size();
        return tableDO;
    }


    private TableDO findTableChecked(Long tableId) throws EntityNotFoundException {
        TableDO tableDO = tableRepository.findOne(tableId);
        if (tableDO == null) {
            throw new EntityNotFoundException("Could not find entity with id: " + tableId);
        }
        return tableDO;
    }

    //what is minimal interval  for reservation ?
    private boolean isValidReservation(ReservationDO reservation) {
        ZonedDateTime now = ZonedDateTime.now();
        ZonedDateTime newFromTime = reservation.getTimeslot().getFromTime();
        ZonedDateTime newToTime = reservation.getTimeslot().getToTime();
        return newFromTime.isBefore(newToTime) && newFromTime.isAfter(now) && newToTime.isAfter(now);
    }

    private boolean isValidReservationForTable(List<ReservationDO> reservations, ReservationDO reservation) {
        ZonedDateTime newFromTime = reservation.getTimeslot().getFromTime();
        ZonedDateTime newToTime = reservation.getTimeslot().getToTime();
        for (ReservationDO tableReservation : reservations) {
            ZonedDateTime fromTime = tableReservation.getTimeslot().getFromTime();
            ZonedDateTime toTime = tableReservation.getTimeslot().getToTime();

            if ((isAfterOrEquals(newFromTime, fromTime) && newFromTime.isBefore(toTime))
                    || (isAfterOrEquals(newToTime, fromTime) && isBeforeOrEquals(newToTime, toTime))) {
                return false;
            }
        }
        return true;
    }

    private boolean isAfterOrEquals(ZonedDateTime fromTimeFirst, ZonedDateTime fromTimeSecond) {
        return fromTimeFirst.isAfter(fromTimeSecond) || fromTimeFirst.isEqual(fromTimeSecond);
    }

    private boolean isBeforeOrEquals(ZonedDateTime fromTimeFirst, ZonedDateTime fromTimeSecond) {
        return fromTimeFirst.isBefore(fromTimeSecond) || fromTimeFirst.isEqual(fromTimeSecond);
    }
}
