package com.orderbird.service;

import com.orderbird.entity.ReservationDO;
import com.orderbird.entity.TableDO;
import com.orderbird.exception.ConstraintsViolationException;
import com.orderbird.exception.EntityNotFoundException;
import com.orderbird.exception.NotValidReservationTime;
import com.orderbird.exception.TableAlreadyInUseException;

public interface TableService {

    TableDO find(Long tableId) throws EntityNotFoundException;

    TableDO create(TableDO tableDO) throws ConstraintsViolationException;

    void makeReservation(Long tableId, ReservationDO reservationDO) throws EntityNotFoundException, TableAlreadyInUseException, NotValidReservationTime;

    TableDO findTableWithReservations(Long tableId) throws EntityNotFoundException;
}
