package com.orderbird.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class OrderBirdSecurityConfig extends WebSecurityConfigurerAdapter {
    //    Info About Security here: https://docs.spring.io/spring-security/site/docs/current/reference/html/headers.html
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        //        We could also prevent framing of content to the log in page using java configuration:
        http.headers().frameOptions().disable();
        //        http.headers().frameOptions().sameOrigin();
    }
}
