package com.orderbird.dao;

import com.orderbird.entity.TableDO;
import org.springframework.data.repository.CrudRepository;

public interface TableRepository extends CrudRepository<TableDO, Long> {

}
